import UIKit


public let deviceID: String? = UserDefaults.standard.value(forKey: kDeviceToken) as? String
public let kDeviceToken = "DeviceToken"
