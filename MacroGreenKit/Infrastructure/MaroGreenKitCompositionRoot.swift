import Foundation

public class MacroGreenKitCompositionRoot {

    public static var sharedInstance: MacroGreenKitCompositionRoot = MacroGreenKitCompositionRoot()

    private init() {}
    
    public lazy var resolveProfileAPI: ProfileAPI = {
        return ProfileAPIImpl.init()
    }()
    
    
    

    
    /*public lazy var resolveJobAPI: JobAPI = {
        return JobAPIImpl.init()
    }()
    
    public lazy var resolveProfileAPI: ProfileAPI = {
        return ProfileAPIImpl.init()
    }()
    
    public lazy var resolveNotificationAPI: NotificationAPI = {
        return NotificationAPIImpl.init()
    }()
    
    public lazy var resolveSkillsAPI: SkillsAPI = {
        return SkillsAPIImpl.init()
    }()
    
    public lazy var resolveGetAppVersionAndCheckIt: GetAppVersionAndCheckIt = {
        return GetAppVersionAndCheckItImpl.init()
    }()
    
    public lazy var resolveShowAd: ShowAd = {
        return ShowAdImpl.init()
    }()
    */
}
