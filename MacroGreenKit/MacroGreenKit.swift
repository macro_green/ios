import Foundation


public class MacroGreenKit {
    public static func start(basePath: String, apiBasePath: String) {
        variables = ["base_path": apiBasePath,
                     "api_base_path": apiBasePath]
    }
    
    static var variables: [String: String]?
}

class MacroGreen {
    static var baseEndpoint: String {
        return MacroGreenKit.variables!["base_path"] ?? "http://85.119.150.103:3000"
    }
    
    static var apiEndpoint: String {
        return MacroGreenKit.variables!["api_base_path"] ?? "http://85.119.150.103:3000/api"
    }
}
