import UIKit

var sceneWindow: UIWindow?

func checkNotificationCenter(completion: @escaping (Bool) -> Void) {
    UNUserNotificationCenter.current().getNotificationSettings { settings in
        if settings.authorizationStatus == .denied {
            completion(false)
        } else if settings.authorizationStatus == .authorized {
            completion(true)
        } else if settings.authorizationStatus == .notDetermined {
            completion(false)
        } else {
            completion(false)
        }
    }
}

func openURL(url: String, completion: ((Bool) -> Void)?) {
    if let link = URL(string: url) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIApplication.shared.open(link, options: [:], completionHandler: completion)
        }
    }
}
