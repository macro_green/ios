//
//  ControlViewController.swift
//  MacroGreen
//
//  Created by Tanya Koldunova on 01.02.2021.
//

import UIKit

class ControlViewController: BaseViewController<ControlViewModel>, ControlView, UITextFieldDelegate {

    @IBOutlet weak var arrow2: UIImageView!
    @IBOutlet weak var arrow: UIImageView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var vector: UIImageView!
    @IBOutlet weak var controlNameLabel: UILabel!
    @IBOutlet weak var startTextField: UITextField! {
        didSet {
            startTextField.delegate = self
        }
    }
    @IBOutlet weak var endTextField: UITextField! {
        didSet {
            endTextField.delegate = self
        }
    }
    
//    public var titleName: String?
//    public var mainImage: UIImage?
//    public var checkImage: UIImage?
//    public var color: UIColor?
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
       
        
        controlNameLabel.text = viewModel.title
        vector.image = viewModel.mainImage
        arrow.image = viewModel.checkImage
        saveButton.layer.backgroundColor = viewModel.color.cgColor
        customTextField(textField: startTextField)
        customTextField(textField: endTextField)
        getShadowArrow(image: arrow)
        getShadowArrow(image: arrow2)
        navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func goBack(_ sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func customTextField (textField: UITextField) {
        textField.borderStyle = .none
        textField.layer.cornerRadius = 10
        textField.layer.masksToBounds = true
        textField.layer.backgroundColor = UIColor.white.cgColor
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        heightConstraint.constant = 40.0
//        UIView.animate(withDuration: 0.3) {
//                self.view.layoutIfNeeded()
//        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
//            heightConstraint.constant = 80.0
//            UIView.animate(withDuration: 0.3) {
//                    self.view.layoutIfNeeded()
//            }
    }
    
    func getShadowArrow (image:UIImageView) {
        image.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        image.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        image.layer.shadowOpacity = 1.0
        image.layer.shadowRadius = 4
        image.layer.masksToBounds = false
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


