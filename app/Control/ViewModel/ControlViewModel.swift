//
//  ControlViewModel.swift
//  MacroGreen
//
//  Created by Tanya Koldunova on 01.02.2021.
//

import UIKit
import MacroGreenKit

protocol ControlView: BaseAsedoUI {
//    func showLoadingAnimation()
//    func hideLoadingAnimation()
//    func reloadSkillsCollectionView()
}



class ControlViewModel: ViewModel {
    
    let view: ControlView
    var title: String
    var mainImage: UIImage
    var checkImage: UIImage
    var color: UIColor
    
    init(view:ControlView, title: String, mainImage: UIImage, checkImage: UIImage, color:UIColor) {
        self.view = view
        self.title = title
        self.mainImage = mainImage
        self.checkImage = checkImage
        self.color = color
    }
}
