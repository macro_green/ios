//
//  CustomView.swift
//  testStackView
//
//  Created by Tanya Koldunova on 28.01.2021.
//

import UIKit

class CustomView: UIView {

    let nibName = "CustomView"
   // var titleName: String?
    var mainImage: UIImage?
    var checkImage: UIImage?
    var color: UIColor?
    
   // @IBOutlet var contentView: UIView!
    var contenView: UIView?
    @IBOutlet weak var controlButton: UIButton!
    @IBOutlet weak var switchIndicator: UISwitch!
    @IBOutlet weak var statusOnImage: UIImageView!
    @IBOutlet weak var statusOfImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var signLabel: UILabel!
    @IBOutlet weak var unitsLabel: UILabel!
    
    
    override init(frame: CGRect) {
           super.init(frame: frame)
          //initSubviews()
        setupThisView()
        
       }

    
    private func setupThisView(){
        
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 25.0
        
        
        view.layer.shadowColor = UIColor(red: 0.796, green: 0.796, blue: 0.795, alpha: 0.5).cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowOpacity = 1.0
        view.layer.shadowRadius = 20.0
        view.layer.masksToBounds = false
        
        
        
        controlButton.isHidden = true
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contenView = view
        
        }

        func loadViewFromNib() -> UIView? {
           let nibName = String(describing: CustomView.self)
            let bundle = Bundle(for: type(of: self))
            let nib = UINib(nibName: nibName, bundle: bundle)
            return nib.instantiate(withOwner: self,options: nil).first as? UIView
            
            
        }

        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            setupThisView()
            
          }

    func configureView(title:String?, image1: ImageAsset?, image2: ImageAsset?, mainImage1: UIImage, checkImage1: UIImage, color1: UIColor){
        titleLabel.text = title
        statusOnImage.image = image1?.image
        statusOfImage.image = image2?.image
        mainImage = mainImage1
        checkImage = checkImage1
        color = color1
    }
    
    @IBAction func clickOnSwitch(_ sender: Any) {
        if !switchIndicator.isOn {
            controlButton.isHidden = false
        }
        else {
            controlButton.isHidden = true 
            
        }
    }
    
    @IBAction func clickOnControllButton(_ sender: Any) {
        if let t = titleLabel.text{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2){
                Coordinator.shared.goToControlViewController(title: t, mainImage: self.mainImage!, checkImage: self.checkImage!, color: self.color!)
//                sceneWindow?.rootViewController = UINavigationController(rootViewController: CompositionRoot.sharedInstance.resolveControlViewController(title: t, mainImage: self.mainImage!, checkImage: self.checkImage!, color: self.color!))
                
            }
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
