import Foundation
import MapKit
import CoreLocation

extension CLLocationCoordinate2D {
    private func toDegrees(_ number: Double) -> Double {
        return number * 180 / .pi
    }
    
    func angleFrom(coord: CLLocationCoordinate2D) -> Double {
        
        let dLon = (coord.longitude - longitude);
        
        let y = sin(dLon) * cos(coord.latitude);
        let x = cos(latitude) * sin(coord.latitude) - sin(latitude) * cos(coord.latitude) * cos(dLon);
        
        var brng = atan2(y, x);
        
        brng = toDegrees(brng)
        brng = (brng + 360).truncatingRemainder(dividingBy: 360.0)
        
        return brng;
    }
    
    func distanceFrom(coord: CLLocationCoordinate2D) -> Double {
//                let dX = pow(2, (coord.latitude - latitude))
//                let dY = pow(2, (coord.longitude - longitude))
//                let distance = pow(1/2, (dX + dY))
//
//                return distance
        let location = CLLocation(latitude: coord.latitude, longitude: coord.longitude)
        let distance = location.distance(from: CLLocation(latitude: latitude, longitude: longitude))

        return distance/10000
    }
}
