import UIKit
//import Firebase

//let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)

var isFirstLaunch: Bool {
    set {
        UserDefaults.standard.set(!newValue, forKey: "isFirstLaunch")
    }
    get {
        return !UserDefaults.standard.bool(forKey: "isFirstLaunch")
    }
}

var isHandoffClipboard: Bool {
    set {
        UserDefaults.standard.set(newValue, forKey: "isHandoffClipboard")
    }
    get {
        return UserDefaults.standard.bool(forKey: "isHandoffClipboard")
    }
}

var isLiveSearch: Bool {
    set {
        UserDefaults.standard.set(newValue, forKey: "isLiveSearch")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "isLiveSearch"), object: newValue)
    }
    get {
        return UserDefaults.standard.bool(forKey: "isLiveSearch")
    }
}

//public func isAnyPurchased() -> Bool {
//    var isPurchased: [Bool] = [Bool]()
//    for product in TrackerProducts.productIDs {
//        isPurchased.append(TrackerProducts.store.isProductPurchased(product))
//    }
//    return isPurchased.contains(true)
//}
//JDncvwnvciencience
