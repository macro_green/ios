import Foundation

extension Date {

    func currentTimeInMilliSeconds() -> Int {
        let since1970 = self.timeIntervalSince1970
        return Int(since1970 * 1000)
    }

}
