import Foundation

extension Date {
    static func seconds(date: String) -> Double? {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "E, dd MMM yyyy HH:mm:ssZZZZZ"
        
        if let time = dateFormatterGet.date(from: date) {
            let timeInSec = time.timeIntervalSince1970
//            let timeInSec =
//                86400 * Calendar.current.component(.day, from: time) +
//                86400 * Calendar.current.component(.day, from: time) +
//                3600 * Calendar.current.component(.hour, from: time) +
//                60 * Calendar.current.component(.minute, from: time) +
//                    Calendar.current.component(.second, from: time)
            return timeInSec
        } else {
            return nil
        }
    }
}
