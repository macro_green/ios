import Foundation

extension Array {
    public func any(_ predicate: @escaping (Element) -> Bool) -> Bool {
        return reduce(false, { (acc, element) -> Bool in
            predicate(element) || acc
        })
    }

    public func all(_ predicate: @escaping (Element) -> Bool) -> Bool {
        guard count > 0 else { return false }

        return reduce(true, { (acc, element) -> Bool in
            predicate(element) && acc
        })
    }

    public func keepLast(_ n: Int = 1) -> Array {
        if count > n {
            return Array(dropFirst(count - n))
        } else {
            return self
        }
    }
}
