import Foundation

extension Data {
    func transform<T>(with decoder: JSONDecoder, to model: T.Type) throws -> T where T: Decodable {
        return try decoder.decode(model, from: self)
    }
}
