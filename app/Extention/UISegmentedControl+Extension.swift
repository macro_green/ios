import Foundation
import  UIKit

extension UISegmentedControl {

    func setSelectedSegmentColor(foregroundColor: UIColor, tintColor: UIColor) {
        if #available(iOS 13.0, *) {
            self.layer.backgroundColor = UIColor.white.cgColor
            self.setTitleTextAttributes([.foregroundColor: foregroundColor], for: .selected)
            self.selectedSegmentTintColor = tintColor

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                for i in 0...(self.numberOfSegments-1) {
                    let backgroundSegmentView = self.subviews[i]
                    backgroundSegmentView.isHidden = true
                }
            }
        } else { self.tintColor = tintColor }
    }
}
