import UIKit

extension UIView {

    func border(color: UIColor = ColorName.default,
                width: CGFloat = 1.0) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }

    func cornerRadius(radius: CGFloat = 10.0) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
    }

    func circleCornerRadius() {
        self.clipsToBounds = true
        self.layer.cornerRadius = self.frame.height/2
    }

    func shadow(opacity: Float = 0.3,
                offset: CGSize = CGSize(width: 0, height: 1),
                radius: CGFloat = 3,
                color: UIColor = .gray) {
        self.clipsToBounds = false
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shadowColor = color.cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds,
                                             cornerRadius: self.layer.cornerRadius).cgPath
    }

    func setShadowWithColor(color: UIColor?, opacity: Float?, offset: CGSize?, shadowRadius: CGFloat, viewCornerRadius: CGFloat?) {
        layer.shadowColor = color?.cgColor ?? UIColor.black.cgColor
        layer.shadowOpacity = opacity ?? 1.0
        layer.shadowOffset = offset ?? CGSize.zero
        layer.shadowRadius = shadowRadius
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }

    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }

    func roundCorners(cornerRadius: Double) {
         let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
         let maskLayer = CAShapeLayer()
         maskLayer.frame = self.bounds
         maskLayer.path = path.cgPath
         self.layer.mask = maskLayer
     }

    func roundedCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }

    func addShadow(to edges: [UIRectEdge], radius: CGFloat = 3.0, opacity: Float = 0.5, color: CGColor = UIColor.gray.cgColor) {
           let fromColor = color
           let toColor = UIColor.clear.cgColor
           let viewFrame = self.frame
           for edge in edges {
               let gradientLayer = CAGradientLayer()
               gradientLayer.colors = [fromColor, toColor]
               gradientLayer.opacity = opacity

               switch edge {
               case .top:
                   gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
                   gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
                   gradientLayer.frame = CGRect(x: 0.0, y: 0.0, width: viewFrame.width, height: radius)
               case .bottom:
                   gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
                   gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
                   gradientLayer.frame = CGRect(x: 0.0, y: viewFrame.height - radius, width: viewFrame.width, height: radius)
               case .left:
                   gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                   gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                   gradientLayer.frame = CGRect(x: 0.0, y: 0.0, width: radius, height: viewFrame.height)
               case .right:
                   gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.5)
                   gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.5)
                   gradientLayer.frame = CGRect(x: viewFrame.width - radius, y: 0.0, width: radius, height: viewFrame.height)
               default:
                   break
               }
               self.layer.addSublayer(gradientLayer)
           }
       }

       func removeAllShadows() {
           if let sublayers = self.layer.sublayers, !sublayers.isEmpty {
               for sublayer in sublayers {
                   sublayer.removeFromSuperlayer()
               }
           }
       }

      var isVisibleToUser: Bool {

            if isHidden || alpha == 0 || superview == nil {
                return false
            }

            guard let rootViewController = UIApplication.shared.keyWindow?.rootViewController else {
                return false
            }

            let viewFrame = convert(bounds, to: rootViewController.view)

            let topSafeArea: CGFloat
            let bottomSafeArea: CGFloat

            if #available(iOS 11.0, *) {
                topSafeArea = rootViewController.view.safeAreaInsets.top
                bottomSafeArea = rootViewController.view.safeAreaInsets.bottom
            } else {
                topSafeArea = rootViewController.topLayoutGuide.length
                bottomSafeArea = rootViewController.bottomLayoutGuide.length
            }

            return viewFrame.minX >= 0 &&
                viewFrame.maxX <= rootViewController.view.bounds.width &&
                viewFrame.minY >= topSafeArea &&
                viewFrame.maxY <= rootViewController.view.bounds.height - bottomSafeArea

        }
    }
