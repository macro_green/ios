//
//  HomeScreenViewController.swift
//  praticeCV
//
//  Created by Nasta Koldunova on 26.01.2021.
//

import UIKit

class HomeScreenViewController: BaseViewController<HomeViewModel>, HomeView {

    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var identifyButton: UIButton!
   
    @IBOutlet weak var spiciesButton: UIButton!
    
    @IBOutlet weak var articlesButton: UIButton!
    
    @IBOutlet weak var profilePhoto: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customSearchBar(searchBar: searchBar, backgroundColor: Asset.searchBarColor.color, textcolor: UIColor.white, buttonColor: UIColor.white)
        setImage(image: profilePhoto)
        makeButtonShadow(button: identifyButton)
        makeButtonShadow(button: spiciesButton)
        makeButtonShadow(button: articlesButton)
        
        setButton(button: identifyButton, backgroundColor: Asset.greenColor.color, image: Asset.whiteCamera.image, titleColor: UIColor.white)
        
        
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setImage (image : UIImageView) {
           image.layer.borderWidth = 1
           image.layer.masksToBounds = false
           image.layer.borderColor = UIColor.black.cgColor
           image.layer.cornerRadius = image.frame.height/2
           image.clipsToBounds = true
    }
    
    fileprivate func makeButtonShadow(button: UIButton) {
        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        button.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 0.0
        button.layer.masksToBounds = false
        button.layer.cornerRadius = 4.0
    }
    
    
    @IBAction func spiciesButtonClick(_ sender: Any) {
        /* spicies = !spicies
         if spicies {*/
        setButton(button: spiciesButton, backgroundColor: Asset.greenColor.color, image: Asset.whiteLeaf.image, titleColor: UIColor.white)
        setButton(button: articlesButton, backgroundColor:UIColor.white, image: Asset.book.image, titleColor: Asset.greenColor.color)
        setButton(button: identifyButton, backgroundColor: UIColor.white, image: Asset.camera.image, titleColor: Asset.greenColor.color)
        /*} else {
         setButton(button: spiciesButton, backgroundColor: UIColor.white, image: UIImage(named: "leaf")!, titleColor: UIColor(named: "green_color")!)
         }*/
    }
    
    @IBAction func articleButtonClick(_ sender: Any) {
        /*article = !article
         if article {*/
        setButton(button: articlesButton, backgroundColor:Asset.greenColor.color, image: Asset.whiteBook.image, titleColor: UIColor.white)
        setButton(button: spiciesButton, backgroundColor: UIColor.white, image: Asset.leaf.image, titleColor: Asset.greenColor.color)
        setButton(button: identifyButton, backgroundColor: UIColor.white, image: Asset.camera.image, titleColor: Asset.greenColor.color)
        /* } else {
         setButton(button: articlesButton, backgroundColor:UIColor.white, image: UIImage(named: "book")!, titleColor: UIColor(named: "green_color")!)
         }*/
    }
    
    @IBAction func identifyButtonClick(_ sender: Any) {
        /*  identify = !identify
         if identify {*/
        setButton(button: identifyButton, backgroundColor: Asset.greenColor.color, image: Asset.whiteCamera.image, titleColor: UIColor.white)
        setButton(button: articlesButton, backgroundColor:UIColor.white, image: Asset.book.image, titleColor: Asset.greenColor.color)
        setButton(button: spiciesButton, backgroundColor: UIColor.white, image: Asset.leaf.image, titleColor: Asset.greenColor.color)
        /* } else {
         setButton(button: identifyButton, backgroundColor: UIColor.white, image: UIImage(named: "camera")!, titleColor: UIColor(named: "green_color")!)
         }*/
    }
    
    fileprivate func setButton (button : UIButton, backgroundColor : UIColor, image : UIImage, titleColor : UIColor) {
        button.backgroundColor = backgroundColor
        button.setImage(image, for: UIControl.State.normal)
        button.setTitleColor(titleColor, for: .normal)
    }
    
    fileprivate func customSearchBar(searchBar:UISearchBar, backgroundColor: UIColor, textcolor:UIColor, buttonColor:UIColor){
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField{
        textfield.backgroundColor = backgroundColor
        textfield.textColor = textcolor
            if let leftView = textfield.leftView as? UIImageView {
                leftView.tintColor = buttonColor
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
