//
//  HomeViewModel.swift
//  MacroGreen
//
//  Created by Tanya Koldunova on 27.01.2021.
//

import Foundation
import MacroGreenKit

protocol HomeView: BaseAsedoUI {
//    func showLoadingAnimation()
//    func hideLoadingAnimation()
//    func reloadSkillsCollectionView()
}



class HomeViewModel: ViewModel {
    
    let view: HomeView
    
    init(view:HomeView) {
        self.view = view
    }
}

