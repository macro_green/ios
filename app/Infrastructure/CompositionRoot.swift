import UIKit
import MacroGreenKit


class CompositionRoot {
    
    static var sharedInstance: CompositionRoot = CompositionRoot()
    
    var rootTabBarController: UITabBarController!
    
    var macroGreenKitCompositionRoot: MacroGreenKitCompositionRoot {
        return MacroGreenKitCompositionRoot.sharedInstance
    }
    
    required init() {
        configureRootTabBarController()
    }
    
    private func configureRootTabBarController() {
            rootTabBarController = UITabBarController()
            rootTabBarController.tabBar.isTranslucent = false

            var viewControllersList: [UIViewController] = [UIViewController]()
            
            let firstVC = resolveMonitorViewController()
            firstVC.tabBarItem = UITabBarItem(title: "", image: Asset.monitorIcon.image, tag: 0)

            viewControllersList.append(firstVC)

            let secondVC = resolveSelectPlantsViewController()
            secondVC.tabBarItem = UITabBarItem(title: "", image: Asset.selectPlantsIcon.image, tag: 1)

            viewControllersList.append(secondVC)

            let thirdVC = resolveProfileViewController()
            thirdVC.tabBarItem = UITabBarItem(title: "", image: Asset.homePage.image, tag: 2)

            viewControllersList.append(thirdVC)
            let viewControllers = viewControllersList.map { (viewController) -> UIViewController in
                let navigationController = UINavigationController(rootViewController: viewController)
                //navigationController.navigationBar.isHidden = true
                return navigationController
            }
            
            rootTabBarController.setViewControllers(viewControllers, animated: true)
        }
    
    
    // MARK: ViewControllers
    
//    func resolveHomeViewController() -> HomeViewController {
//        let vc = HomeViewController.instantiateFromStoryboard("Home")
//        vc.viewModel = resolveHomeViewModel(view: vc)
//        return vc
//    }
//
//    func resolveSplashScreenViewController () -> SplashScreenViewController {
//        let vc = SplashScreenViewController.instantiateFromStoryboard("SplashScreen")
//        vc.viewModel = resolveSplashScreenViewModel(view: vc)
//        return vc
//    }
//
    func resolveIntroductionViewController() -> IntroductionViewController {
        let vc = IntroductionViewController.instantiateFromStoryboard("Introduction")
//        vc.viewModel = resolveIntroductionViewModel(view: vc)
        return vc
    }

    
//    func resolveControlViewController() -> ControlViewController {
//        let vc = ControlViewController.instantiateFromStoryboard("Control")
//        vc.viewModel = resolveControlModel(view: vc)
//        return vc
//    }
    
    func resolveControlViewController(title:String, mainImage: UIImage, checkImage: UIImage, color: UIColor) -> ControlViewController {
        let vc = ControlViewController.instantiateFromStoryboard("Control")
//        vc.titleName = title
//        vc.mainImage = mainImage
//        vc.checkImage = checkImage
//        vc.color = color
        
        vc.viewModel = resolveControlModel(view: vc, title: title, mainImage: mainImage, checkImage: checkImage, color: color)
        return vc
    }
    
    func resolveOnboardingViewController() -> OnboardingViewController {
        let vc = OnboardingViewController.instantiateFromStoryboard("Onboarding")
//        vc.viewModel = resolveIntroductionViewModel(view: vc)
        return vc
    }
    
    func resolveLoginViewController() -> LoginViewController {
        let vc = LoginViewController.instantiateFromStoryboard("Login")
        vc.viewModel = resolveLoginViewModel(view: vc)
        return vc
    }

    func resolveSignUpViewController() -> SignUpViewController {
        let vc = SignUpViewController.instantiateFromStoryboard("SignUp")
        vc.viewModel = resolveSignUpViewModel(view: vc)
        return vc
    }
    
    func resolveProfileViewController() -> ProfileViewController {
        let vc = ProfileViewController.instantiateFromStoryboard("Profile")
        vc.viewModel = resolveProfileViewModel(view: vc)
        return vc
    }
    
    func resolveHomeViewController() -> HomeScreenViewController {
        let vc = HomeScreenViewController.instantiateFromStoryboard("Home")
        vc.viewModel = resolveHomeViewModel(view: vc)
        return vc
    }
    
    func resolveSelectPlantsViewController() -> SelectPlantsViewController {
        let vc = SelectPlantsViewController.instantiateFromStoryboard("SelectPlants")
        vc.viewModel = resolveSelectPlantsModel(view: vc)
        return vc
    }
    
//     func resolveTabBarViewController() -> TabBarViewController {
//        let vc = TabBarViewController()
//        return vc
//    }
    
    func resolveMonitorViewController() -> MonitorsViewController {
        let vc = MonitorsViewController.instantiateFromStoryboard("Monitor")
        vc.viewModel = resolveMonitorViewModel(view: vc)
        return vc
    }
    
    
//
//    func resolvePolicyViewController(state: PolicyViewModel.PolicyState) -> PolicyViewController {
//        let vc = PolicyViewController.instantiateFromStoryboard("Policy")
//        vc.viewModel = resolvePolicyViewModel(state: state)
//        return vc
//    }
//
//    func resolvePolicyViewModel(state: PolicyViewModel.PolicyState) -> PolicyViewModel {
//        return PolicyViewModel(state: state)
//    }
//
//    func resolveSubscriptionViewModel(view: SubscriptionView) -> SubscriptionViewModel {
//        return SubscriptionViewModel(view: view)
//    }
//
    func resolveLoginViewModel(view: LoginView) -> LoginViewModel {
        return LoginViewModel(view: view)
    }
    
    func resolveSignUpViewModel(view: SignUpView) -> SignUpViewModel {
        return SignUpViewModel(view: view)
    }
    
    func resolveProfileViewModel(view: ProfileView) -> ProfileViewModel {
        return ProfileViewModel(view: view)
    }

    func resolveHomeViewModel(view: HomeView) -> HomeViewModel {
        return HomeViewModel(view: view)
    }
    
    func resolveMonitorViewModel(view: MonitorView) -> MonitorViewModel {
        return MonitorViewModel(view: view)
    }
    
    func resolveSelectPlantsModel(view: SelectPlantsView) -> SelectPlantsModel {
        return SelectPlantsModel(view: view)
    }
    
    func resolveControlModel(view: ControlView, title:String, mainImage: UIImage, checkImage: UIImage, color: UIColor) -> ControlViewModel {
        return ControlViewModel(view: view, title: title, mainImage: mainImage, checkImage: checkImage, color: color)
    }

    
//
//    func resolveIntroductionViewModel(view: IntroductionView) -> IntroductionViewModel {
//        return IntroductionViewModel(view: view, profileAPI: trackerKitCompositionRoot.resolveProfileAPI)
//    }
//
//    func resolveHomeViewModel(view: HomeView) -> HomeViewModel {
//        return HomeViewModel(view: view, profileAPI: trackerKitCompositionRoot.resolveProfileAPI, jobAPI: trackerKitCompositionRoot.resolveJobAPI, motificationAPI: trackerKitCompositionRoot.resolveNotificationAPI)
//    }
//
//    func resolveSplashScreenViewModel(view: SplashScreenView) -> SplashScreenModel {
//        return SplashScreenModel(view: view, update: trackerKitCompositionRoot.resolveGetAppVersionAndCheckIt)
//    }
    
}
