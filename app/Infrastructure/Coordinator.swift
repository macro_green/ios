import UIKit

class Coordinator {
    
    static let shared = Coordinator()
    
    private var compositionRoot: CompositionRoot {
        return CompositionRoot.sharedInstance
    }

    private var lastPresentedViewController: UIViewController?
    
    private var baseNavigationController: UINavigationController? {
        get {
            if let navigationController = lastPresentedViewController?.navigationController {
                return navigationController
            } else {
                return sceneWindow?.rootViewController as? UINavigationController
            }
        }
    }
    
    private var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    private init() { }
    
    func showRootTabBarController() {
        let tabBarController = compositionRoot.rootTabBarController
        let navController = UINavigationController(rootViewController: compositionRoot.rootTabBarController)
        navController.navigationBar.isHidden = true
        tabBarController?.view.window?.rootViewController = navController
    }
    
    func goToSignUpViewController() {
        push(compositionRoot.resolveSignUpViewController())
    }
    
//    func goToControlViewController() {
//        push(compositionRoot.resolveControlViewController())
//    }
    
    func goToControlViewController(title:String, mainImage: UIImage, checkImage: UIImage, color: UIColor) {
        push(compositionRoot.resolveControlViewController(title: title, mainImage: mainImage, checkImage: checkImage, color: color))
    }
    
    func goToTabBarController() {
        push(compositionRoot.rootTabBarController)
    }
    
//
//    func goToSubscriptionViewController() {
//        push(compositionRoot.resolveSubscriptionViewController())
//    }
//
//    func goToPolicyViewController(state: PolicyViewModel.PolicyState) {
//        present(compositionRoot.resolvePolicyViewController(state: state))
//    }
//
    
    func push(_ vc: UIViewController) {
        if let baseNavigationController = baseNavigationController {
            baseNavigationController.pushViewController(vc, animated: true)
            lastPresentedViewController = vc
        } else if let tabBarController = sceneWindow?.rootViewController as? UITabBarController, let navigationController = tabBarController.selectedViewController as? UINavigationController {
           // navigationController.navigationBar.isHidden = true
            navigationController.pushViewController(vc, animated: true)
        }
        else if let navigationController = appDelegate.window?.rootViewController as? UINavigationController {
            //navigationController.navigationBar.isHidden = true
            navigationController.pushViewController(vc, animated: true)
        }
    }
    
    func present(_ vc: UIViewController) {
        if let baseNavigationController = baseNavigationController {
            baseNavigationController.pushViewController(vc, animated: true)
            lastPresentedViewController = vc
        } else {
            if let navigationController = appDelegate.window?.rootViewController as? UINavigationController {
                navigationController.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func setNavigationsetViewController(_ vc: UIViewController) {
        if let baseNavigationController = baseNavigationController {
            baseNavigationController.setViewControllers([vc], animated: true)
            lastPresentedViewController = vc
        } else {
            if let navigationController = appDelegate.window?.rootViewController as? UINavigationController {
                navigationController.setViewControllers([vc], animated: true)
            }
        }
    }
    
}
