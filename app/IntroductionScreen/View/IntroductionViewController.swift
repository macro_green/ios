//
//  IntroductionViewController.swift
//  MacroGreen
//
//  Created by Tanya Koldunova on 25.01.2021.
//

import UIKit
import AVFoundation

class IntroductionViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!{
        didSet {
            scrollView.delegate = self
        }
    }
    @IBOutlet weak var nextButtonPressed: UIButton!
    
    @IBOutlet weak var oneScreenWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var oneScreenWidth: NSLayoutConstraint!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        let frame = view.frame
        oneScreenWidthConstraint.constant =  frame.width

        // Do any additional setup after loading the view.
    }
    
    private func scrollToIndex(index: Int) {
        let frame = view.frame
        let rect = CGRect(x: CGFloat(index) * oneScreenWidthConstraint.constant,
                          y: 0,
                          width: frame.width,
                          height: frame.height)
        scrollView.scrollRectToVisible(rect, animated: true)
    }

    @IBAction func scrollNextButton1(_ sender: Any) {
        scrollToIndex(index: 1)
    }
    
    @IBAction func scrollNextButton2(_ sender: Any) {
        scrollToIndex(index: 2)
    }
    
    @IBAction func scrollNextButton3(_ sender: Any) {
        print("---------------------button clicked---------------------------")
        self.showLoginController()
        
    }
    
    private func showLoginController() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 ) {
            sceneWindow?.rootViewController = UINavigationController(rootViewController: CompositionRoot.sharedInstance.resolveLoginViewController())
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension IntroductionViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        
//        if pageControl.currentPage == 1 {
//             connectNotificationCenter()
//        }
    }
    
}
