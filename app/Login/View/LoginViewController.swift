//
//  LoginViewController.swift
//  MacroGreenHome
//
//  Created by Nasta Koldunova on 25.01.2021.
//

import UIKit

class LoginViewController: BaseViewController<LoginViewModel>, LoginView, UITextFieldDelegate {
    
    @IBOutlet weak var loginTextField: UITextField!{
        didSet{
            loginTextField.delegate = self
        }
    }
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordTextField: UITextField!{
        didSet{
            passwordTextField.delegate = self
        }
    }
    
    
    @IBOutlet weak var rememberMeButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    var rememberMe = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        passwordTextField.underlined(color: UIColor.lightGray)
        loginTextField.underlined(color: UIColor.lightGray)
        loginButton.layer.cornerRadius = 8.0
        topConstraint.constant = 48
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func rememberMeClick(_ sender: Any) {
        rememberMe = !rememberMe
        rememberMeButton.setImage(rememberMe ? Asset.check.image : nil, for: UIControl.State.normal)
    }
    
    @IBAction func clickOnSignUpButton(_ sender: Any) {
        showSignUpViewController()
    }
    
    @IBAction func clickOnLogin(_ sender: Any) {
        DispatchQueue.main.async {
            //Coordinator.shared.showRootTabBarController()
//           sceneWindow?.rootViewController = UINavigationController(rootViewController: CompositionRoot.sharedInstance.resolveTabBarViewController())
            self.goToTabBar()
        }
        
    }
    private func showSignUpViewController() {
        DispatchQueue.main.async {
           // sceneWindow?.rootViewController = UINavigationController(rootViewController: CompositionRoot.sharedInstance.resolveSignUpViewController())
            Coordinator.shared.goToSignUpViewController()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.loginTextField{
            loginTextField.underlined(color: Asset.greenColor.color)
            topConstraint.constant = 24
            UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
            }
        } else {
            passwordTextField.underlined(color: Asset.greenColor.color)
            topConstraint.constant = 24
            UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
            }
            passwordTextField.isSecureTextEntry = true
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.loginTextField{
            loginTextField.underlined(color: UIColor.lightGray)
            topConstraint.constant = 48
            UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
            }
        } else {
            passwordTextField.underlined(color: UIColor.lightGray)
            topConstraint.constant = 48
            UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
            }
    }
    }
    fileprivate func goToTabBar () {
        sceneWindow?.rootViewController = CompositionRoot.sharedInstance.rootTabBarController
//        Coordinator.shared.goToTabBarController()
    }
//    func addUnderline (color:UIColor, textField:UITextField){
//        let bottomLine = CALayer()
//        bottomLine.frame = CGRect(x: 0, y: view.frame.height - 1, width: view.frame.width, height: 1)
//        bottomLine.backgroundColor = color.cgColor
//        textField.borderStyle = .none
//        textField.layer.addSublayer(bottomLine)
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UITextField {
    
    func underlined(color : UIColor){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
