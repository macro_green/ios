//
//  LoginViewModel.swift
//  MacroGreenHome
//
//  Created by Nasta Koldunova on 25.01.2021.
//

import Foundation
import MacroGreenKit

protocol LoginView: BaseAsedoUI {
//    func showLoadingAnimation()
//    func hideLoadingAnimation()
//    func reloadSkillsCollectionView()
}



class LoginViewModel: ViewModel {
    
    let view: LoginView
    
    init(view:LoginView) {
        self.view = view
    }
}
