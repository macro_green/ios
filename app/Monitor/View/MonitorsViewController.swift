//
//  MonitorsViewController.swift
//  MacroGreen
//
//  Created by Tanya Koldunova on 29.01.2021.
//

import UIKit

class MonitorsViewController: BaseViewController<MonitorViewModel>, MonitorView {

    @IBOutlet weak var aitTemperatureView: CustomView!
    @IBOutlet weak var temperatureWaterView: CustomView!
    
    @IBOutlet weak var airHumidityView: CustomView!
    @IBOutlet weak var lightView: CustomView!
    @IBOutlet weak var pHView: CustomView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        aitTemperatureView.configureView(title: "Температура воздуха", image1: Asset.selectedFan, image2: Asset.radiator, mainImage1: Asset.airVector.image, checkImage1: Asset.airArow.image, color1: Asset.airColor.color)
        
        temperatureWaterView.configureView(title: "Температура воды", image1: Asset.fan, image2: Asset.selectedRadiator, mainImage1: Asset.tempetureVector.image, checkImage1: Asset.temperatureArrow.image, color1: Asset.temperatureColor.color)
        
        airHumidityView.configureView(title: "Влажность воздуха", image1: Asset.waterItem, image2: Asset.waterItem2, mainImage1: Asset.humidityVector.image, checkImage1: Asset.humidityArrow.image, color1: Asset.humidityColor.color)
       
        
        lightView.configureView(title: "Освещение", image1: Asset.idea, image2: Asset.idea2, mainImage1: Asset.lightingVector.image, checkImage1: Asset.lightingArrow.image, color1: Asset.lightningColor.color)
        
        pHView.configureView(title: "ph", image1: Asset.arrowUp, image2: Asset.arrowDown, mainImage1: Asset.phVector.image, checkImage1: Asset.phArrow.image, color1: Asset.phColor.color)
//        if switchControl.isOn{
//            switchControlIsOn = true
//           
//        }
//        else {
//            switchControlIsOn = false
//        }

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)

            self.navigationController?.setNavigationBarHidden(true, animated: false)
        }

        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)

            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
