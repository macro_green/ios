//
//  MonitorViewModel.swift
//  MacroGreen
//
//  Created by Tanya Koldunova on 28.01.2021.
//

import Foundation

protocol MonitorView: BaseAsedoUI {
//    func showLoadingAnimation()
//    func hideLoadingAnimation()
//    func reloadSkillsCollectionView()
}



class MonitorViewModel: ViewModel {
    
    let view: MonitorView
    
    init(view:MonitorView) {
        self.view = view
    }
}
