//
//  OnboardingViewController.swift
//  praticeCV
//
//  Created by Nasta Koldunova on 28.01.2021.
//

import UIKit
import AVFoundation

class OnboardingViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var oneScreenWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!{
        didSet {
            scrollView.delegate = self
        }
    }
    
    @IBOutlet weak var readyButtonPress: UIButton!
    
//    @IBOutlet weak var pageControl: UIPageControl!
    
   
    var application: UIApplication? {
        get {
            return UIApplication.shared
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        
        let frame = view.frame
        oneScreenWidthConstraint.constant =  frame.width
    }
    
    private func scrollToIndex(index: Int) {
        let frame = view.frame
        let rect = CGRect(x: CGFloat(index) * oneScreenWidthConstraint.constant,
                          y: 0,
                          width: frame.width,
                          height: frame.height)
        scrollView.scrollRectToVisible(rect, animated: true)
    }
    
    
    @IBAction func readyButton1(_ sender: Any) {
        scrollToIndex(index: 1)
    }
    
    @IBAction func readyButton2(_ sender: Any) {
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

