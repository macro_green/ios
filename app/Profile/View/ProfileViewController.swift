//
//  ProfileViewController.swift
//  MacroGreen
//
//  Created by Tanya Koldunova on 26.01.2021.
//

import UIKit

class ProfileViewController: BaseViewController<ProfileViewModel>, ProfileView {

    @IBOutlet weak var atriclesButton: UIButton!
    @IBOutlet weak var speciesButton: UIButton!
    @IBOutlet weak var likesButton: UIButton!
    @IBOutlet weak var userImage: UIImageView!
    
    var clickOnarticles = false
    var clickOnSpecies = false
    var clickOnLikes = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
         
        userImage.layer.cornerRadius = 0.5 * userImage.bounds.size.width
        
        atriclesButton.layer.cornerRadius = 20.0
        speciesButton.layer.cornerRadius = 20.0
        likesButton.layer.cornerRadius = 20.0
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickOnArticles(_ sender: Any) {
        atriclesButton.layer.backgroundColor = Asset.myBlue.color.cgColor
        atriclesButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        
        speciesButton.layer.backgroundColor = UIColor.clear.cgColor
        speciesButton.setTitleColor(Asset.titleColor.color, for: UIControl.State.normal)
        likesButton.layer.backgroundColor = UIColor.clear.cgColor
        likesButton.setTitleColor(Asset.titleColor.color, for: UIControl.State.normal)
        }
    
    @IBAction func clickOnSpecies(_ sender: Any) {
            speciesButton.layer.backgroundColor = Asset.myBlue.color.cgColor
            speciesButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        
            atriclesButton.layer.backgroundColor = UIColor.clear.cgColor
            atriclesButton.setTitleColor(Asset.titleColor.color, for: UIControl.State.normal)
            likesButton.layer.backgroundColor = UIColor.clear.cgColor
            likesButton.setTitleColor(Asset.titleColor.color, for: UIControl.State.normal)
            
    }
    
    @IBAction func clickOnLikes(_ sender: Any) {
            likesButton.layer.backgroundColor = Asset.myBlue.color.cgColor
            likesButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        
            atriclesButton.layer.backgroundColor = UIColor.clear.cgColor
            atriclesButton.setTitleColor(Asset.titleColor.color, for: UIControl.State.normal)
            speciesButton.layer.backgroundColor = UIColor.clear.cgColor
            speciesButton.setTitleColor(Asset.titleColor.color, for: UIControl.State.normal)
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
