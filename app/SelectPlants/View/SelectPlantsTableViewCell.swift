//
//  SelectPlantsTableViewCell.swift
//  praticeCV
//
//  Created by Nasta Koldunova on 28.01.2021.
//

import UIKit

class SelectPlantsTableViewCell: UITableViewCell {

    @IBOutlet weak var plantImage: UIImageView!
    
    @IBOutlet weak var plantNameLabel: UILabel!
    
    @IBOutlet weak var plantFamilyLabel: UILabel!
    
    @IBOutlet weak var plantTypeLabel: UILabel!
    
    
    @IBOutlet weak var additionallyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configure (plantImage:UIImage, plantName:String, plantFamily:String, plantType:String, additionally:String){
        self.plantImage.image = plantImage
        plantNameLabel.text = plantName
        plantFamilyLabel.text = plantFamily
        plantTypeLabel.text = plantType
        additionallyLabel.text = additionally
    }

}
