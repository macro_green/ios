//
//  SelectPlantsViewController.swift
//  praticeCV
//
//  Created by Nasta Koldunova on 28.01.2021.
//

import UIKit

class SelectPlantsViewController: BaseViewController<SelectPlantsModel>, SelectPlantsView {    //BaseViewController<SelectPlantsModel>, SelectPlantsView /*, UITableViewDelegate, UITableViewDataSource*/{
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SelectPlantsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectPlantCell", for: indexPath) as! SelectPlantsTableViewCell
        cell.configure(plantImage: Asset.redCactus.image, plantName: "Красный кактус", plantFamily: "кактус", plantType: "from hell", additionally: "something else")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 ) {
            sceneWindow?.rootViewController = UINavigationController(rootViewController: CompositionRoot.sharedInstance.resolveOnboardingViewController())
        }
    }
    
}
