//
//  SelectPlantsModel.swift
//  MacroGreen
//
//  Created by Tanya Koldunova on 28.01.2021.
//

import Foundation
import MacroGreenKit

protocol SelectPlantsView : BaseAsedoUI{
    
}


class SelectPlantsModel: ViewModel {
    
    let view: SelectPlantsView
    
    init (view : SelectPlantsView){
        self.view = view
    }
}
