//
//  SignUpViewController.swift
//  MacroGreenHome
//
//  Created by Nasta Koldunova on 25.01.2021.
//

import UIKit

class SignUpViewController: BaseViewController<SignUpViewModel>, SignUpView, UITextFieldDelegate {
   
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField! {
        didSet {
        passwordTextField.delegate = self
        }
    }
    
    var acceptNews = false
    var showPassword = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signUpButton.layer.cornerRadius = 25.0
       // Do any additional setup after loading the view.
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == passwordTextField {
            passwordTextField.isSecureTextEntry = true
        }
        }
    
    @IBAction func acceptClick(_ sender: Any) {
        acceptNews = !acceptNews
        acceptButton.setImage(acceptNews ? Asset.check.image : nil , for: UIControl.State.normal)
    }
    
    
    @IBAction func signUpClick(_ sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func showPassword(_ sender: Any) {
        showPassword = !showPassword
        if showPassword == true {
            passwordTextField.isSecureTextEntry = true
        } else {
            passwordTextField.isSecureTextEntry = false
        }
    }
    
    
    @IBAction func loginButton(_ sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
