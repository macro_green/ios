//
//  SignUpViewModel.swift
//  MacroGreenHome
//
//  Created by Nasta Koldunova on 25.01.2021.
//

import Foundation
import MacroGreenKit

protocol SignUpView: BaseAsedoUI {
//    func showLoadingAnimation()
//    func hideLoadingAnimation()
//    func reloadSkillsCollectionView()
}

class SignUpViewModel: ViewModel {
    
    let view: SignUpView
    
    init(view: SignUpView) {
        self.view = view
    }
}
