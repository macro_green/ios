// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit
  internal enum ColorName { }
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit
  internal enum ColorName { }
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Colors

// swiftlint:disable identifier_name line_length type_body_length
internal extension ColorName {
  /// 0x920047ff (r: 146, g: 0, b: 71, a: 255)
  internal static let `default` = #colorLiteral(red: 0.57254905, green: 0.0, blue: 0.2784314, alpha: 1.0)
  /// 0x80b654ff (r: 128, g: 182, b: 84, a: 255)
  internal static let defaultGreen = #colorLiteral(red: 0.5019608, green: 0.7137255, blue: 0.32941177, alpha: 1.0)
  /// 0xfaf2f6ff (r: 250, g: 242, b: 246, a: 255)
  internal static let defaultLight = #colorLiteral(red: 0.98039216, green: 0.9490196, blue: 0.9647059, alpha: 1.0)
  /// 0x219653ff (r: 33, g: 150, b: 83, a: 255)
  internal static let green = #colorLiteral(red: 0.12941177, green: 0.5882353, blue: 0.3254902, alpha: 1.0)
  /// 0xf6f6f6ff (r: 246, g: 246, b: 246, a: 255)
  internal static let lightGray = #colorLiteral(red: 0.9647059, green: 0.9647059, blue: 0.9647059, alpha: 1.0)
  /// 0x6fcf97ff (r: 111, g: 207, b: 151, a: 255)
  internal static let lightGreen = #colorLiteral(red: 0.43529412, green: 0.8117647, blue: 0.5921569, alpha: 1.0)
  /// 0xde3434ff (r: 222, g: 52, b: 52, a: 255)
  internal static let red = #colorLiteral(red: 0.87058824, green: 0.20392157, blue: 0.20392157, alpha: 1.0)
  /// 0xeceeeeff (r: 236, g: 238, b: 238, a: 255)
  internal static let skillBackground = #colorLiteral(red: 0.9254902, green: 0.93333334, blue: 0.93333334, alpha: 1.0)
  /// 0x6f7179ff (r: 111, g: 113, b: 121, a: 255)
  internal static let skillText = #colorLiteral(red: 0.43529412, green: 0.44313726, blue: 0.4745098, alpha: 1.0)
}
// swiftlint:enable identifier_name line_length type_body_length
