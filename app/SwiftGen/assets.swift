// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let accentColor = ColorAsset(name: "AccentColor")
  internal static let image1 = ImageAsset(name: "Image-1")
  internal static let introductionImage2 = ImageAsset(name: "IntroductionImage2")
  internal static let acceptImage = ImageAsset(name: "acceptImage")
  internal static let airArow = ImageAsset(name: "airArow")
  internal static let airColor = ColorAsset(name: "airColor")
  internal static let airVector = ImageAsset(name: "airVector")
  internal static let arrowDown = ImageAsset(name: "arrowDown")
  internal static let arrowUp = ImageAsset(name: "arrowUp")
  internal static let blackgroundColor = ColorAsset(name: "blackgroundColor")
  internal static let book = ImageAsset(name: "book")
  internal static let camera = ImageAsset(name: "camera")
  internal static let check = ImageAsset(name: "check")
  internal static let controlArrow = ImageAsset(name: "controlArrow")
  internal static let controlButton = ImageAsset(name: "controlButton")
  internal static let controlRectangle = ImageAsset(name: "controlRectangle")
  internal static let elips = ImageAsset(name: "elips")
  internal static let elips2 = ImageAsset(name: "elips2")
  internal static let elipsForMonitor = ImageAsset(name: "elipsForMonitor")
  internal static let elipsForMonitor2 = ImageAsset(name: "elipsForMonitor2")
  internal static let elipsForMonitor3 = ImageAsset(name: "elipsForMonitor3")
  internal static let elipsForMonitor4 = ImageAsset(name: "elipsForMonitor4")
  internal static let elipse = ImageAsset(name: "elipse")
  internal static let fan = ImageAsset(name: "fan")
  internal static let feather = ImageAsset(name: "feather")
  internal static let greenColor = ColorAsset(name: "green_color")
  internal static let homePage = ImageAsset(name: "homePage")
  internal static let humidityArrow = ImageAsset(name: "humidityArrow")
  internal static let humidityColor = ColorAsset(name: "humidityColor")
  internal static let humidityVector = ImageAsset(name: "humidityVector")
  internal static let idea = ImageAsset(name: "idea")
  internal static let idea2 = ImageAsset(name: "idea2")
  internal static let introductionImage1 = ImageAsset(name: "introductionImage1")
  internal static let introductionImage3 = ImageAsset(name: "introductionImage3")
  internal static let leaf = ImageAsset(name: "leaf")
  internal static let lightingArrow = ImageAsset(name: "lightingArrow")
  internal static let lightingVector = ImageAsset(name: "lightingVector")
  internal static let lightningColor = ColorAsset(name: "lightningColor")
  internal static let loc = ImageAsset(name: "loc")
  internal static let location = ImageAsset(name: "location")
  internal static let monitorIcon = ImageAsset(name: "monitorIcon")
  internal static let myBlue = ColorAsset(name: "myBlue")
  internal static let nitSolutionColor = ColorAsset(name: "nitSolutionColor")
  internal static let noPhoto = ImageAsset(name: "noPhoto")
  internal static let nutSlutionVector = ImageAsset(name: "nutSlutionVector")
  internal static let nutSolutionArrow = ImageAsset(name: "nutSolutionArrow")
  internal static let phArrow = ImageAsset(name: "pHArrow")
  internal static let phColor = ColorAsset(name: "pHColor")
  internal static let phVector = ImageAsset(name: "phVector")
  internal static let profile = ImageAsset(name: "profile")
  internal static let profileBackground = ImageAsset(name: "profileBackground")
  internal static let profilePhoto = ImageAsset(name: "profilePhoto")
  internal static let radiator = ImageAsset(name: "radiator")
  internal static let rectangle = ImageAsset(name: "rectangle")
  internal static let redCactus = ImageAsset(name: "red_cactus")
  internal static let rememberMe = ImageAsset(name: "rememberMe")
  internal static let searchBarColor = ColorAsset(name: "searchBarColor")
  internal static let selectPlantsIcon = ImageAsset(name: "selectPlantsIcon")
  internal static let selectedFan = ImageAsset(name: "selectedFan")
  internal static let selectedRadiator = ImageAsset(name: "selectedRadiator")
  internal static let temperatureArrow = ImageAsset(name: "temperatureArrow")
  internal static let temperatureColor = ColorAsset(name: "temperatureColor")
  internal static let tempetureVector = ImageAsset(name: "tempetureVector")
  internal static let titleColor = ColorAsset(name: "titleColor")
  internal static let userImage = ImageAsset(name: "userImage")
  internal static let waterItem = ImageAsset(name: "waterItem")
  internal static let waterItem2 = ImageAsset(name: "waterItem2")
  internal static let whiteBook = ImageAsset(name: "white_book")
  internal static let whiteCamera = ImageAsset(name: "white_camera")
  internal static let whiteLeaf = ImageAsset(name: "white_leaf")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = {
    guard let color = Color(asset: self) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }()

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init?(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
}

internal extension ImageAsset.Image {
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init?(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
